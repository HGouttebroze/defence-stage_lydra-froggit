module.exports = {
  siteTitle: "Froggit",
  siteDescription: "",
  authorName: "Lydra",
  twitterUsername: "art_devops",
  authorAvatar: "avatar.jpeg", // file in content/images
  defaultLang: "fr", // show flag if lang is not default. Leave empty to enable flags in post lists
  authorDescription: `
  
  `,
  //siteUrl: "https://maxpou.github.io/",
  //siteUrl: "https://lydra.fr/blog/",
  siteUrl: "https://lydra.fr/tag/froggit/feed/",
  //disqusSiteUrl: "https://www.maxpou.fr/",
  // Prefixes all links. For cases when deployed to maxpou.fr/gatsby-starter-morning-dew/
  pathPrefix: "/defence-stage_lydra-froggit", // Note: it must *not* have a trailing slash.
  siteCover: "cover-baymax.jpeg", // file in content/images
  googleAnalyticsId: "UA-67868977-2",
  background_color: "#ffffff",
  theme_color: "#222222",
  display: "standalone",
  icon: "content/images/logo_froggit.png",
  postsPerPage: 4,
  disqusShortname: "maxpou",
  headerTitle: "Froggit",
  headerLinksIcon: "logo_froggit.png", //  (leave empty to disable: '')
  headerLinks: [
    {
      label: "Accueil",
      url: "/",
    },
    {
      label: "Fonctionnalités",
      url: "/",
    },
    {
      label: "FAQ",
      url: "/faq",
    },
    {
      label: "Tarifs",
      url: "/tarifs",
    },
    
    {
      label: "Blog",
      url: "/blog",
    },
    
    {
      label: "Login",
      url: "/login",
    },
    {
      label: "S'enregistrer",
      url: "/enregistrement",
    },
  ],
  // Footer information (ex: Github, Netlify...)
  websiteHost: {
    name: "GitLab",
    url: "https://gitlab.com",
  },
  footerLinks: [
    {
      sectionName: "Pourquoi Froggit ?",
      links: [
        {
          label: "Ton code source est-il en sécurité ?",
          url: "https://lydra.fr/es-2-ton-code-source-est-il-vraiment-en-securite/",
        },
        {
          label: "La souveraineté numérique",
          url: "https://lydra.fr/category/souverainete-numerique/",
        },
      ],
    },
    {
      sectionName: "Communauté",
      links: [
        {
          label: "Forum",
          url: "/forum",
        },
        {
          label: "Wiki",
          url: "/wiki",
        },
      ],
    },
    {
      sectionName: "Société",
      links: [
        {
          label: "À propos",
          url: "/a_propos",
        },
        {
          label: "Contact",
          url: "/contact",
        },
        
        {
          label: "Équipe",
          url: "/equipe",
        },
        {
          label: "Mentions légales",
          url: "/mentions_legales",
        },
        {
          label: "CGV",
          url: "https://oxalis-scop.fr/conditions-generales-de-vente",
        },
        {
          label: "CPV",
          url: "/cpv",
        },
        {
          label: "CGU",
          url: "/cgu",
        },
      ],
    },
  ],
}
