# How to create new page ?

We have two principals steps to make for create a new page in this Gatsby Theme. We can also create a new page in a sub menu.

## First step

This theme is configure to work by default with a template page files under `content/pages/`.

So, we need to create a file in `content/pages/` with the `.mdx` extention. This file will become the new page.

## Second step

We have to go in `gatsby-config.js` file.
Next, in the siteMetada, we have to add or rename the menuLinks values.

### name value

`name` is the value that will emerge in the site header

### link value

`link` correspond to the name's page of the file we had create in the first step, in the template page `content/pages/...`

### type value

`type` can be :
  - `internal` : use to create a link to an other page of the site.
  - `anchor` : use for a fast navigation in the same page, it's create links between a source anchor and a destination anchor.

`type` will determinate if it is an internal page of the site or if it is an anchor link. 

### we need to provide a structure like this

```
menuLinks: [
      {
        name: `file-name`,
        link: `/link-name`,
        type: `internal`, //internal or anchor
      },
```

#### Add a Sub-Menu

We can add a sub-menu into the menuLinks, like this exemple:

```
menuLinks: [
      {
        name: `file-name`,
        link: `/link-name`,
        type: `internal`, //internal or anchor
        subMenu: [
          {
            name: `sub-file-name`,
            link: `/sub-link-name`,
            type: `internal`, //internal or anchor
          },
        ],
      },
    ],
```
