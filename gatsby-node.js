const path = require(`path`)
const { createFilePath } = require("gatsby-source-filesystem")

/*
const { load, createFeed } = require('./internals')
async function sourceNodes({ boundActionCreators }, options = {}) {
  const { createNode } = boundActionCreators
  const { feeds = [] } = options

  for (const f of feeds) {
    const { rss } = await load(f)

    if (rss && rss.channel) {
      const sources = (Array.isArray(rss.channel)
        ? rss.channel : [rss.channel]
      )

      sources.forEach(f => createFeed(f, createNode))
    }
  }

  return Promise.resolve()
}

module.exports = {
  sourceNodes,
}
*/



exports.createPages = async ({ graphql, actions, reporter }) => {
  const { createPage } = actions

  const BlogPostTemplate = require.resolve("./src/templates/blog-post.js")
  const BlogPostShareImage = require.resolve(
    "./src/templates/blog-post-share-image.js"
  )
  const PageTemplate = require.resolve("./src/templates/page.js")
  const PostsBytagTemplate = require.resolve("./src/templates/tags.js")
  const ListPostsTemplate = require.resolve(
    "./src/templates/blog-list-template.js"
  )

  const allMarkdownQuery = await graphql(`
  {
    allMarkdown: allMdx(
      sort: { fields: [frontmatter___date], order: DESC }
      filter: { frontmatter: { published: { ne: false } } }
      limit: 1000
    ) {
      edges {
        node {
          fileAbsolutePath
          frontmatter {
            title
            slug
            tags
            language
            cover {
              publicURL
            }
            unlisted
          }
          timeToRead
          excerpt
        }
      }
      totalCount
    }
  }
`)

  if (allMarkdownQuery.errors) {
    reporter.panic(allMarkdownQuery.errors)
  }

  const postPerPageQuery = await graphql(`
    {
      site {
        siteMetadata {
          postsPerPage
        }
      }
    }
  `)

  const markdownFiles = allMarkdownQuery.data.allMarkdown.edges

  const posts = markdownFiles.filter(item =>
    item.node.fileAbsolutePath.includes("/content/posts/")
  )

  const listedPosts = posts.filter(
    item => item.node.frontmatter.unlisted !== true
  )
  // pour créer des pages /posts :
  /* on a 1 fonction qui est disponible, createPage, on l'appel en asyn car on a 1 requête qui 
  est executée (en asyn.await car c'est de asyncrone), ça nous donne des valeurs qui sont disponibles :
  ({ actions, graphql }), 
  on attent le résultat de la requête qu'on va mettre ds data (const { data })

  exports createPages = async function ({ actions, graphql })
    const { data } = await graphql(`
    
    `)
  1. on fait notre requête GraphQL (on pt la tester ds graphiQL):

    ce qui nous intéresse est de réqupérer ttes les pages markdown, avec:
      allMarkdownRemark
    dans le but de récupérer le slug (il va ns permettre de créer la page) :
      edges {
        node {
          fields {
            slug
          }
        }
      }
2. on va allé chercher ds ma data le allMarkdownRemark, edges, et on va aller parcourir ce 
tableau (avec 1 forEach) pour créer les pages:

data.allMarkdownRemark.edges.forEach() edge => {
  const { slug } = edge.node.fields
  actions.createPage({
    path: slug,
    component: require.resolve('./src/templates/posts.js'),
    context: {slug, slug},
  })
}
)

2.1. on récupère le slug ci dessus ( avec la fonction : const { slug } ), 
    on utilise la createPage de l'ojet action pr créer la page au niveau programmatique
    on lui met en path le slug (unique & nom du fichier) qu'on a recupérer juste avant ds la requete
    on va chercher le template (posts.js)
    on lui passe le context qui sont des objets qu'on peut passer au template, 
    on lui passe le slug car la requete executé ds le template aura besoin pour trouver la page unique

  */

/*
 exports.createPages = async function ({ actions, graphql }) {
 const { data } = await graphql(`
  allMarkdownRemark {
    edges {
      node {
        fields {
          slug
        }
      }
    }
  }
`)
data.allMarkdownRemark.edges.forEach( edge => {
  const { slug } = edge.node.fields
  actions.createPage({
    path: slug,
    component: require.resolve('./src/templates/posts.js'),
    context: {slug, slug},
  })
})
}
*/

  // create index post
  /* POUR CREER LA LISTE DES POSTES:
    on fait une création de page (createPage), ds laquelle on passe le path (on a choisi de 
    faire un blog, la page d'accueil sera /blog, et on veut donc y retrouver la liste des posts),
    puis, dans "component", on va chercher le template où on a créé la liste les posts;
    Enfin, nous n'avons pas besoin de "context" pr l'instant.
  
Code: 

  actions.createPage({
    path: '/blog',
    component: require.resolve(`./src/templates/list.js`),
    context: {},
  })
*/

// Ajout de la Pagination

  //const postsPerPage = postPerPageQuery.data.site.siteMetadata.postsPerPage
 // const nbPages = Math.ceil(listedPosts.length / postsPerPage)
 /*
const perPage = 2;
const nbPages = Math.ceil(data.allMarkdownRemark. totalCount / perPage)

for (let i=0; i< nbPages; i++){


actions.createPage({
  path: i < 1 ? "/blog" : `/blog/${i + 1}`,
  //path: '/blog',
  component: require.resolve(`./src/templates/list.js`),
  context: {
    limit: perPage,
    skip: i * perPage,
  },
})
}
*/

  // generate paginated post list
  const postsPerPage = postPerPageQuery.data.site.siteMetadata.postsPerPage
  const nbPages = Math.ceil(listedPosts.length / postsPerPage)

  Array.from({ length: nbPages }).forEach((_, i) => {
    createPage({
      
      path: i === 0 ? `/blog` : `/pages/${i + 1}`,
      component: ListPostsTemplate,
      context: {
        limit: postsPerPage,
        skip: i * postsPerPage,
        currentPage: i + 1,
        nbPages: nbPages,
      },
    })
  })


  // generate blog posts
  
  posts.forEach((post, index, posts) => {
    const previous = index === posts.length - 1 ? null : posts[index + 1].node
    const next = index === 0 ? null : posts[index - 1].node

    createPage({
      path: post.node.frontmatter.slug,
      component: BlogPostTemplate,
      context: {
        slug: post.node.frontmatter.slug,
        previous,
        next,
      },
    })

    // generate post share images (dev only)
    if (process.env.gatsby_executing_command.includes("develop")) {
      createPage({
        path: `${post.node.frontmatter.slug}/image_share`,
        component: BlogPostShareImage,
        context: {
          slug: post.node.frontmatter.slug,
          width: 440,
          height: 220,
        },
      })
    }
  })



  // generate pages
  markdownFiles
    .filter(item => item.node.fileAbsolutePath.includes("/content/pages/"))
    .forEach(page => {
      createPage({
        path: page.node.frontmatter.slug,
        component: PageTemplate,
        context: {
          slug: page.node.frontmatter.slug,
        },
      })
    })

  // generate tag page
  markdownFiles
    .filter(item => item.node.frontmatter.tags !== null)
    .reduce(
      (acc, cur) => [...new Set([...acc, ...cur.node.frontmatter.tags])],
      []
    )
    .forEach(uniqTag => {
      createPage({
        path: `tags/${uniqTag}`,
        component: PostsBytagTemplate,
        context: {
          tag: uniqTag,
        },
      })
    })
  }

  /*
  let Parser = require('rss-parser');
  let parser = new Parser();
  
  (async () => {
  
    let feed = await parser.parseURL('https://lydra.fr/tag/froggit/feed/rss.xml');
    console.log(feed.title);
  
    feed.items.forEach(item => {
      console.log(item.title + ':' + item.link)
    });
  
  })();
*/

  /*
  exports.createResolvers = ({
    actions,
    cache,
    createNodeId,
    createResolvers,
    store,
    reporter,
  }) => {
    const { createNode } = actions
    createResolvers({
      CMS_Asset: {
        imageFile: {
          type: `File`,
          resolve(source, args, context, info) {
            return createRemoteFileNode({
              url: source.url,
              store,
              cache,
              createNode,
              createNodeId,
              reporter,
            })
          },
        },
      },
    })
  }
  */

//exports.onCreateNode = function onCreateNode({ pathPrefix, basePath = pathPrefix }) {
   //use basePath to replace links as you wish
  // e.g. you would visit each a tag, and replace the href with basePath + href
 //}

exports.onCreateNode = ({ node, actions, getNode }) => {
  const { createNodeField } = actions

  if (node.internal.type === `MarkdownRemark`) {
    const value = createFilePath({ node, getNode })
    createNodeField({
      name: `slug`,
      node,
      value,
      //value: `/blog${slug}`,
    })
  }}
/*
    if (node.internal.type === `MarkdownRemark`) {
      const fileNode = getNode(node.parent)
      let nodeSlug
      nodeSlug = ensureSlashes(
        path.basename(fileNode.relativePath, path.extname(fileNode.relativePath))
      )
      if (nodeSlug) {
        createNodeField({ node, name: `slug`, value: nodeSlug })
      }
    }
  
  function ensureSlashes(slug) {
    if (slug.charAt(0) !== `/`) {
      slug = `/` + slug
    }
  
    if (slug.charAt(slug.length - 1) !== `/`) {
      slug = slug + `/`
    }
  
    return slug
  }
 
  }
}
*/

// gatsby-source-rss
/*
const parser = require('rss-parser');
const crypto = require('crypto');

const createContentDigest = obj => crypto.createHash('md5').update(JSON.stringify(obj)).digest('hex');

function promisifiedParseURL(url) {
  return new Promise((resolve, reject) => {
    parser.parseURL(url, (err, data) => {
      if (err) {
        reject(err);
      }
      resolve(data.feed);
    });
  });
}

const createChildren = (entries, parentId, createNode) => {
  const childIds = [];
  entries.forEach(entry => {
    childIds.push(entry.link);
    const node = Object.assign({}, entry, {
      id: entry.link,
      title: entry.title,
      link: entry.link,
      description: entry.description,
      parent: parentId,
      children: []
    });
    node.internal = {
      type: 'rssFeedItem',
      contentDigest: createContentDigest(node)
    };
    createNode(node);
  });
  return childIds;
};

async function sourceNodes({ boundActionCreators }, { rssURL }) {
  const { createNode } = boundActionCreators;
  const data = await promisifiedParseURL(rssURL);
  if (!data) {
    return;
  }
  const { title, description, link, entries } = data;
  const childrenIds = createChildren(entries, link, createNode);
  const feedStory = {
    id: link,
    title,
    description,
    link,
    parent: null,
    children: childrenIds
  };

  feedStory.internal = { type: 'rssFeed', contentDigest: createContentDigest(feedStory) };

  createNode(feedStory);
}

exports.sourceNodes = sourceNodes;
*/
