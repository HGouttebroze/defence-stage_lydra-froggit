---
title: this post is a ghost
slug: invisible-post
date: 2020-01-01

# optional fields
published: true
unlisted: false
generate-card: false
language: fr
cover: ./cover.jpeg

tags: ['fake']
translations:
  - link: 'https://www.maxpou.fr/about'
    language: 'french'
    hreflang: fr
---

This exists to populate GraphQL fields and avoid null errors. It should contain
all of the available frontmatter.

link: 'http://lydra.test/category/podcast/feed'
