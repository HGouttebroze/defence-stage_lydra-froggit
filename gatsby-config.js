const path = require("path")
const config = require("./data/siteConfig")

module.exports = {
  siteMetadata: {
    title: ``,
    author: config.authorName,
    description: config.siteDescription,
    siteUrl: config.siteUrl,
    ...config,
  },
  pathPrefix: `/defence-stage_lydra-froggit`,
  plugins: [
    /*
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        name: "posts",
        path: "content/posts",
      },
    },
    */
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        name: "pages",
        path: "content/pages",
      },
    },
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        name: "posts",
        path: "content/posts",
        //path: "src/page/blog",
      },
    },
    
    /*
   {
      resolve: `gatsby-source-filesystem`,
      options: {
        name: 'src',
        //path: "content/posts",
        path: '${__dirname}/src/page/blog.js',
      },
    },
    */

    {
      resolve: `gatsby-source-filesystem`,
      options: {
        name: "images",
        path: "content/images",
      },
    },
    {
      resolve: `gatsby-plugin-page-creator`,
      options: {
        path: path.join(__dirname, `src`, `pages`),
        //path: path.join(__dirname, `src`, `pages`, ),
       // ignore: [`blog.(js|ts)?(x)`],
      },
    },
   /* {
      resolve: `gatsby-plugin-page-creator`,
      options: {
        path: `${__dirname}/src/settings/pages`,
      },
   }, */ 
    {
      resolve: `gatsby-plugin-mdx`,
      options: {
        extensions: [`.mdx`, `.md`],
        defaultLayouts: {
          
          default: require.resolve("./src/templates/page.js"),
          //default: require.resolve("./src/pages/blog.js"),
          
        },
        gatsbyRemarkPlugins: [
          {
            resolve: "gatsby-remark-images",
            options: {
              maxWidth: 590,
              linkImagesToOriginal: false,
              withWebp: true,
            },
          },
          { resolve: "gatsby-remark-prismjs" },
          { resolve: "gatsby-remark-responsive-iframe" },
          { resolve: "gatsby-remark-copy-linked-files" },
          { resolve: "gatsby-remark-smartypants" },
          { resolve: "gatsby-remark-autolink-headers" },
        ],
      },
    },
    // Reminder (https://github.com/gatsbyjs/gatsby/issues/15486#issuecomment-509405867)
    {
      resolve: `gatsby-transformer-remark`,
      options: {
        plugins: [`gatsby-remark-images`],
      },
    },
    `gatsby-transformer-sharp`,
    `gatsby-plugin-offline`,
    `gatsby-plugin-styled-components`,
    `gatsby-plugin-react-helmet`,
    `gatsby-plugin-sharp`,
    {
      resolve: `gatsby-plugin-google-analytics`,
      options: {
        trackingId: config.googleAnalyticsId,
      },
    },

    
    {
      resolve: `gatsby-source-rss-feed`,
      options: {
        url: `https://lydra.fr/tag/froggit/feed/rss.xml`,
        name: `Lydra`,
        // Optional
        // Read parser document: https://github.com/bobby-brennan/rss-parser#readme
        parserOption: {
          customFields: {
            item: ['itunes:duration']
          }
        }

      
      }
    },


/*
{ 
  resolve: '@uptimeventures/gatsby-source-rss',
  options: {
    feeds: ['https://www.uptime.ventures/blog/rss.xml'],
  },
},
*/

  /*
                {
                        resolve: 'gatsby-source-rss',
                        options: {
                                rssURL: 'https://lydra.fr/tag/froggit/feed/rss.xml'
                        }
                },
  */

 /*
    {
      resolve: `gatsby-plugin-feed`,
      options: {
        setup: (options) => ({
          ...options,
          custom_namespaces: {
            yournamespace: "https://lydra.fr/tag/froggit/feed/",
          },
        }),
        feeds: [
          {
            serialize: ({
              query: { allMarkdownRemark },
            }) => {
              return allMarkdownRemark.edges.map((edge) => {
                return Object.assign(
                  {},
                  edge.node.frontmatter,
                  {
                    
                    custom_elements: [
                      //{ "content:encoded": edge.node.html },
                      { "yournamespace:yourcustomfield": edge.node.fields.someField },
                    ],
                  }
                );
              });
            },
          },
        ],
        
      },
    },

*/

/*
{
  resolve: 'gatsby-source-rss',
  options: {
    rssURL: 'https://blog.jordanrhea.com/rss.xml',
    customFields: {
      item: ['tags'],
    },
  },
},
*/

{
  resolve: `gatsby-plugin-feed`,
  options: {
    query: `
      {
        site {
          siteMetadata {
            title
            description
            siteUrl
            site_url: siteUrl
          }
        }
      }
    `,
    feeds: [
      {
        serialize: ({ query: { site, allMarkdownRemark } }) => {
          return allMarkdownRemark.edges.map(edge => {
            return Object.assign({}, edge.node.frontmatter, {
              description: edge.node.excerpt,
              date: edge.node.frontmatter.date,
              url: site.siteMetadata.siteUrl + edge.node.fields.slug,
              guid: site.siteMetadata.siteUrl + edge.node.fields.slug,
              custom_elements: [{ "content:encoded": edge.node.html }],
            })
          })
        },
        query: `
          {
            allMarkdownRemark(
              sort: { order: DESC, fields: [frontmatter___date] },
            ) {
              edges {
                node {
                  excerpt
                  html
                  fields { slug }
                  frontmatter {
                    title
                    date
                  }
                }
              }
            }
          }
        `,
        output: "/rss.xml",
        title: "Your Site's RSS Feed",
      },
    ],
  },
},

    {
      resolve: `gatsby-source-podcast-rss-feed`,
      options: {
        feedURL: `https://lydra.fr/ea-2-le-podcasteur-erwan/rss`,
        id: 'guid',
      },
  },
  
/*
  {
    resolve: `@arshad/gatsby-theme-podcast-core`,
    options: {
      feedUrl: `https://blog.jordanrhea.com/rss.xml`,
      podcast: {
        name: `Name of Podcast`,
        description: `Eligendi nisi nobis nisi voluptate. Corporis deserunt provident hic numquam. Veritatis vero necessitatibus adipisci cumque voluptate rerum at.`,
        image: `content/images/podcast.jpg`,
        social: [
          {
            name: `Apple Podcast`,
            url: `https://itunes.apple.com`,
          },
          {
            name: `Google Podcast`,
            url: `https://podcasts.google.com`,
          },
        ],
      },
    },
  },
*/

/*
  {
    resolve: `gatsby-source-youtube`,
    options: {
      channelId: '<<Youtube channelID eg. UCK8sQmJBp8GCxrOtXWBpyEA >>',
      apiKey: '<< Add your Youtube api key here>>',
      maxVideos: 50 // Defaults to 50
    },
  },
  */

    {
      resolve: `gatsby-plugin-manifest`,
      options: {
        name: config.siteTitle,
        short_name: config.siteTitle,
        start_url: config.pathPrefix,
        background_color: config.background_color,
        theme_color: config.theme_color,
        display: config.display,
        icon: "content/images/logo_froggit.png",
      },
    },
    // https://www.gatsbyjs.org/docs/themes/converting-a-starter/#transpiling-your-theme-with-webpack
    {
      resolve: "gatsby-plugin-compile-es6-packages",
      options: {
        modules: ["gatsby-starter-morning-dew"],
      },
    },
  ],
}
