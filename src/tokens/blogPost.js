/*

query MyQuery {
    site(siteMetadata: {siteUrl: {eq: "https://lydra.fr/tag/froggit/feed/"}}) {
      id
      siteMetadata {
        siteTitle
        siteUrl
        title
      }
    }
  }

  */
 /*
 import React from "react"
 import { graphql, Link } from "gatsby"
 
 import Layout from "../components/layout"
 //import image from ".."
 import SEO from "../components/SEO"
 import PostsList from "../components/PostsList"
 import Wrapper from "../components/Wrapper"
 
 import styled from "styled-components"
 //import ContentHeader from "./ContentHeader"
 //import { colors } from "../tokens"
 //import MDXRenderer from "gatsby-plugin-mdx/mdx-renderer"
 
 //je recupere ttes mes datas
 //puis ds le Layout, je fais 1 boucle sur le noeud (nodes) 
 //pr récupérer le title en h2, l'extrait de texte (excerpt) en p,
 //et on créé 1 lien (Link), avec le lien de l'article en question, le titre pr le texte, 
 //& on utilise Link qui est disponible au niveau de Gatsby, donc on l'importe (au niveau de graphql car les 2 sont dispo au niveau de Gatsby)
 //le Link permet de router à travers l'application,
 const BlogPosts = ({data}) => {
     const { nodes } = data.allMarkdownRemark 
     return (
         <Layout>
             <SEO title="Blog home" />
             
             
             <Wrapper> 
                
 <h1>List posts</h1>
 
             {nodes.map((e) => (
                 <div>
                     <h2>{e.frontmatter.title}</h2>
					 <h2>{e.frontmatter.date}</h2>
					 <h2>{e.frontmatter.cover.relativePath}</h2>
                     <p>{e.edges.node.excerpt}</p>
					 
                     <p>{e.edges.node.rawMarkdownBody}</p>
					 
					 <p>{e.edges.node.fields.slug}</p>
                     <p>{e.tags}</p>
                     <Link to={e.fields.slug}>{e.frontmatter.title}</Link>
                 </div>
 
             ))}
 
             </Wrapper>
         </Layout>
     )
 }
 
 export const query = graphql`
 query BlogPosts {
    
 
	allMarkdownRemark(
		sort: { order: DESC, fields: [frontmatter___date] }
		filter: {
			frontmatter: {
				fileAbsolutePath: { regex: "//content/posts//" }
				frontmatter: { published: { ne: false }, unlisted: { ne: true } 
			}
		}
	) {
		edges {
			node {
				excerpt
				rawMarkdownBody
				fields {
					slug
				}
				frontmatter {
					title
					date
					cover {
						relativePath
					}
					
				}
			}
		}
	}
}`
*/
