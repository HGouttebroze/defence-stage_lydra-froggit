/*
import React from "react"
import { graphql, Link } from "gatsby"

import Layout from "../components/layout"
//import image from ".."
import SEO from "../components/SEO"
import PostsList from "../components/PostsList"
import PostsListItem from "../components/PostsListItem"
import Wrapper from "../components/Wrapper"

import styled from "styled-components"
//import ContentHeader from "./ContentHeader"
//import { colors } from "../tokens"
//import MDXRenderer from "gatsby-plugin-mdx/mdx-renderer"

//je recupere ttes mes datas
//puis ds le Layout, je fais 1 boucle sur le noeud (nodes) 
//pr récupérer le title en h2, l'extrait de texte (excerpt) en p,
//et on créé 1 lien (Link), avec le lien de l'article en question, le titre pr le texte, 
//& on utilise Link qui est disponible au niveau de Gatsby, donc on l'importe (au niveau de graphql car les 2 sont dispo au niveau de Gatsby)
//le Link permet de router à travers l'application,

class BlogList extends React.Component {
    render() {
      const { title, description } = this.props.data.site.siteMetadata
      const posts = this.props.data.posts.edges
      const { pageContext } = this.props

//const IndexPage = ({data}) => {
    //const { nodes } = data.allMarkdownRemark 
    return (
        <Layout>
            <SEO title="Blog home" />
            
            
            <Wrapper> 
                        
<h1>List posts</h1>

            {nodes.map((e) => (
                <div>
                    <h2>{e.frontmatter.title}</h2>
                    <p>{e.excerpt}</p>
                    <p>{e.timeToRead}</p>
                    <p>{e.tags}</p>
                    <Link to={e.fields.slug}>{e.frontmatter.title}</Link>
                </div>

            ))}



            </Wrapper>
        </Layout>
    )
}

//export const query = graphql`
 
export const Query = graphql`
  query List($skip: Int!, $limit: Int!) {
    site {
      siteMetadata {
        title
        description
      }
    }
    posts: allMdx(
      sort: { fields: [frontmatter___date], order: DESC }
      filter: {
        fileAbsolutePath: { regex: "//content/posts//" }
        frontmatter: { published: { ne: false }, unlisted: { ne: true } }
      }
      limit: $limit
      skip: $skip
    ) {
      edges {
        node {
          excerpt
          timeToRead
          frontmatter {
            title
            tags
            language
            slug
          }
        }
      }
    }
  }
`
//export default IndexPage
*/
