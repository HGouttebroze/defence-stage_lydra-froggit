import React from "react"
import { TitleIconWrapper } from "../elements"

export const TitleIcon = ({children}) => {
    return <TitleIconWrapper>{children}</TitleIconWrapper>
}

export default TitleIcon
