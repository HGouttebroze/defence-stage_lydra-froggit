import React from "react"
import { Link } from "gatsby"
import styled from "styled-components"
import useSiteMetadata from "../hooks/use-site-config"
import { media, colors } from "../tokens"
import { Gitlab } from '@styled-icons/fa-brands/Gitlab'
import { LinkedinSquare } from '@styled-icons/boxicons-logos/LinkedinSquare'
import { Twitter } from '@styled-icons/boxicons-logos/Twitter'
import { Youtube } from '@styled-icons/simple-icons/Youtube'


const FooterWrapper = styled.footer`
  text-align: left;
  padding-top: 30px;
  padding-bottom: 50px;
  background-color: ${colors.primary};
  color: ${colors.textLightest};
  padding-left: 20px;
  padding-right: 20px;
  margin: 0 auto;

  & nav {
    display: flex;
    flex-flow: row wrap;
    align-items: flex-start;
    max-width: 900px;
    margin: 0 auto;

    .footer-col {
      flex: 1 1 auto;
      display: inline-flex;
      flex-direction: column;
      padding: 24px;
    }
  }

  & a {
    color: ${colors.textLightest};
    font-weight: bold;

    &:hover {
      color: ${colors.textLightestHover};
    }
  }

  .footer-col > p {
    margin: 0;
  }

  .footer-title {
    font-size: 0.83em;
    margin: 0 0 1rem;
  }

  .footer-item {
    color: ${colors.textLightest};

    & a {
      padding: 0.25rem 0;
      display: block;
    }
  }

  .footer-item-text {
    padding: 0.1rem 20rem 0rem;
    color: ${colors.textLightest};
    flex-direction: row;
  }

  .footer-header {
    order: 1;
    margin: 0 0.25rem;
    margin-right: 0.25rem;
    padding: 0.25rem;
  }

  .footer-column-items {
    grid-template-columns: 1fr;
    display: grid;
  }

  .footer-item-text-lydra {
    display: inline-block;
    margin-left: 40%;
    margin-top: 5%;
  }

  .footer-link-lydra {
    display: inline-block;
    margin-left: 40%;
    margin-top: 5%;
  }

  .footer-social {
     
    display: inline-block;
    margin-left: 40%;
    margin-bottom: 24px;
  
    padding-left: 20px;
    padding-right: 10rem;

    & a {
      padding: 10px;
    }
  }

  @media (max-width: 564px) {
    .footer-col:first-child {
      width: 100%;
    }
  }
`


const Footer = () => {
  const { authorName, websiteHost, footerLinks } = useSiteMetadata()
    
  const FooterItem = ({ item }) => {
    if (item.url.startsWith("/")) {
      return (
        <span className="footer-item">
          <Link className="footer-link" to={item.url}>
            {item.label}
          </Link>
        </span>
      )
    }
    return (
      <span className="footer-item">
        <a className="footer-link" href={item.url}>
          {item.label}
        </a>
      </span>
    )
  }

  const FooterColumn = ({ column }) => {
    return (
      <div className="footer-col">
        <h3 className="footer-title" key={column.sectionName}>
          {column.sectionName}
        </h3>
        <div className="footer-column-items">
          {column.links.map((item, i) => {
            return <FooterItem item={item} key={`footer-column-item-${i}`} />
          })}
        </div>
      </div>
    )
  }

  return (
    <FooterWrapper>
      
        <div className="footer-social">
          <a href="https://twitter.com/art_devops">
            <Twitter size="30px" />
          </a>

          <a href="https://www.youtube.com/channel/UCauIDghddUNu6Fto1nR9Bmg/?sub_confirmation=1">
            <Youtube size="30px" />
          </a>

          <a href="https://www.linkedin.com/company/lydrafr/">
            <LinkedinSquare size="30px" />
          </a>

          <a href="https://gitlab.com/froggit">
            <Gitlab size="30px" />
          </a> 
        </div>

      <nav>
       
        {footerLinks.map((column, i) => {
          return <FooterColumn column={column} key={`footer-column-${i}`} />
        })}
  
        <div className="footer-col">
          <p className="footer-item-text-lydra">
            <a className="footer-link-lydra" href="https://lydra.fr/">
              {authorName} © {new Date().getFullYear()}
            </a>
          </p>
          <p className="footer-item-text">
            Hébergé avec{" "}
            <span className="footer-heart" role="img" aria-label="Love">
              ❤
            </span>{" "}
            par{" "}
            <a className="footer-link" href={websiteHost.url}>
              {websiteHost.name}
            </a>
            .
          </p>
        </div>

      </nav>
      
    </FooterWrapper>
  )
}

export default Footer
