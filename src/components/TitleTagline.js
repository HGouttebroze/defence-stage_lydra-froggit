import React from "react"
import { TitleTaglineWrapper } from "../elements"

export const TitleTagline = ({children}) => {
    return <TitleTaglineWrapper>{children}</TitleTaglineWrapper>
}

export default TitleTagline
