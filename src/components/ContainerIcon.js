import React from "react"
import { ContainerIconWrapper } from "../elements"

export const ContainerIcon = ({children}) => {
    return <ContainerIconWrapper>{children}</ContainerIconWrapper>
}

export default ContainerIcon
