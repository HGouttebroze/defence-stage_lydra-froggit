import React from "react"
import { Helmet } from "react-helmet"
import styled from "styled-components"
import Header from "./Header"
import Footer from "./Footer"
import "prismjs/themes/prism-tomorrow.css"
import { GlobalStyle } from "./Commons"
import { media } from "../tokens"
import { Container, ContainerIcon, TitleTagline, TitleIcon } from "../components"
import { IconSecurite } from "../components"
import { IconFreedom } from "../components"
import { IconGdpr } from "../components"
import Img from "gatsby-image"
//import { useStaticQuery ,graphql } from "gatsby"
import wave_green_2 from '../../content/images/wave_green_2.svg'
import wave_lightGreen from '../../content/images/wave_lightGreen.svg'
import wave_orange_1 from '../../content/images/wave_orange_1.svg'
import wave_light_puple_1 from '../../content/images/wave_light_puple_1.svg'

const SvgWave = () => <><img src={wave_orange_1} /></>
//const SvgWave_1 = () => <img src={wave_light_puple_1} />
/*
<div className="footer-social">
          <a href="https://twitter.com/art_devops">
            <Twitter size="30px" />
          </a>
</div>
*/

const SiteContent = styled.div`
  margin: 0 0;
  padding-top: 0rem;
  @media ${media.medium} {
    margin: 60px 0;
  }
`
/*
const IconSecuriteWrapper = styled.div`
grid-column: auto auto auto;
grid-row: 2 / 3;
overflow: hidden;
position: relative;

.footer-social {
     
  display: inline-block;
  margin-left: 40%;
  margin-bottom: 24px;

  padding-left: 20px;
  padding-right: 10rem;

  & a {
    padding: 10px;
  }
}
`
*/

/*
<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1440 320">
<path fill="#577018" fill-opacity="1" d="M0,32L26.7,42.7C53.3,53,107,75,160,106.7C213.3,139,267,181,320,176C373.3,171,427,117,480,90.7C533.3,64,587,64,640,90.7C693.3,117,747,171,800,170.7C853.3,171,907,117,960,85.3C1013.3,53,1067,43,1120,69.3C1173.3,96,1227,160,1280,197.3C1333.3,235,1387,245,1413,250.7L1440,256L1440,320L1413.3,320C1386.7,320,1333,320,1280,320C1226.7,320,1173,320,1120,320C1066.7,320,1013,320,960,320C906.7,320,853,320,800,320C746.7,320,693,320,640,320C586.7,320,533,320,480,320C426.7,320,373,320,320,320C266.7,320,213,320,160,320C106.7,320,53,320,27,320L0,320Z">
</path>
</svg>
*/

/*
export const query = graphql`
  query {
    file(relativePath: { eq: "../../content/images/wave_green.svg" }) {
      childImageSharp {
        # Specify the image processing specifications right in the query.
        # Makes it trivial to update as your page's design changes.
        fixed(width: 125, height: 125) {
          ...GatsbyImageSharpFixed
        }
      }
    }
  }
`
*/

/*
export const query = ({ fluid }) => {

  const data = useStaticQuery(graphql`
  query {
    imageSharp (fluid : {originalName: {eq: "eq: "wave_green.svg}}) {
        fluid {
            ...GatsbyImageSharpFluid
        }
    }
}

`) 
*/


class Template extends React.Component {
  render() {
    const { children } = this.props

    return (
      <>
        <Helmet>
          <link
            href="https://fonts.googleapis.com/css?family=Lato:400,700&display=swap"
            rel="stylesheet"
          />
        </Helmet>
        <GlobalStyle />
        <Header />
       
        
        <SvgWave />
       
        
       
        <SiteContent>{children}</SiteContent>

        <Footer />
      </>
    )
  }
}

export default Template
