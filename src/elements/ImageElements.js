import styled from "styled-components"
import { colors, media } from "../tokens"

export const IconSecuriteWrapper = styled.div`
grid-column: auto auto auto;
grid-row: 2 / 3;
overflow: hidden;
position: relative;
`
export const IconFreedomWrapper = styled.div`
    grid-column: 3 / span 10;
    grid-row: 2 / 4;
    overflow: hidden;
    position: relative;
    @media ${media.medium} {
        position: relative;
        grid-column: 2 / span 6;
      }
    }
`
export const IconGdprWrapper = styled.div`
    grid-column: 3 / span 10;
    grid-row: 2 / 4;
    overflow: hidden;
    position: relative;
    @media ${media.medium} {
        position: relative;
        grid-column: 2 / span 6;
      }
    }
`
