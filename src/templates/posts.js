import React from "react"
import { graphql } from "gatsby"

import Layout from "../components/layout"
import SEO from "../components/SEO"

const Posts = ({data}) => {
    const post = data.markdownRemark
    return (
        <Layout>
            <SEO title={post.frontmatter.title} />
            <div>
                <h1>{post.frontmatter.title}</h1>
                <div dangerouslySetInnerHTML= {{ __html: post.html}} />
            </div>

            <Wrapper>
        <article>
          <Content content={page.body} date={page.frontmatter.date} />
        </article>
      </Wrapper>

        </Layout>
    );
}

/* le but est de créé un template de page / posts pour éviter d'avoir à créer un fichier JSX React
à chaque fois qu'on voudra créer une page ou un post, l'utilisateur du site n'aura cas créer ces
pages ou posts en markdown ou en .mdx ( extention du markdown) sans avoir de conaissance en 
JavaScript, en JSX / React. Pour ça, on va créér une programmation dynamyque dans le 
fichier gatsby-node.js avant tout, puis on reviendra sur cette page écrite en JSX.

/* EXPLAIN FOLDER FONCTION QUERY / DATA :
on créé une requête qraphql au niveau query, puis on va injecter plus au les data 
de cette requête, on va utiliser ces data pour afficher le titre, le contenu (HTML), déclarer les 
composants comme le SEO ou le layout (composant de mise en page)*/

export const query = graphql`

{
    allMarkdown: allMdx(
      sort: { fields: [frontmatter___date], order: DESC }
      filter: { frontmatter: { published: { ne: false } } }
      limit: 1000
    ) {
      edges {
        node {
          fileAbsolutePath
          frontmatter {
            title
            slug
            tags
            language
            cover {
              publicURL
            }
            unlisted
          }
          timeToRead
          excerpt
        }
      }
    }
  }
`
export default Posts
