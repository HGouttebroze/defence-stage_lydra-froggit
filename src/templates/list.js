import React from "react"
import { graphql, Link } from "gatsby"

import Layout from "../components/layout"
//import image from ".."
import SEO from "../components/SEO"
import PostsList from "../components/PostsList"
import PostsListItem from "../components/PostsListItem"
import Pagination from "../components/Pagination"
import PrevNextPost from "../components/PrevNextPost"
import Wrapper from "../components/Wrapper"
import { colors, media } from "../tokens";

import styled from "styled-components"
//import ContentHeader from "./ContentHeader"
//import MDXRenderer from "gatsby-plugin-mdx/mdx-renderer"

//je recupere ttes mes datas
//puis ds le Layout, je fais 1 boucle sur le noeud (nodes) 
//pr récupérer le title en h2, l'extrait de texte (excerpt) en p,
//et on créé 1 lien (Link), avec le lien de l'article en question, le titre pr le texte, 
//& on utilise Link qui est disponible au niveau de Gatsby, donc on l'importe (au niveau de graphql car les 2 sont dispo au niveau de Gatsby)
//le Link permet de router à travers l'application,
/*
export const WrapperArticle = styled.div`
    padding: 5rem 1rem 5rem 1rem;
    gap: 0 2rem; 
    margin: "auto auto auto auto", 
`
;

export const ContainerWrapper = styled.div`
    display: grid;
    padding: 5rem 1rem 5rem 1rem;
    gap: 0 2rem; 
    margin: "auto auto auto auto", 
`
;

*/
const IndexPage = ({data}) => {
    const { nodes, pageInfo} = data.allMarkdownRemark 
    //, pageInfo, pageContext,, currentPage
    return (
        <Layout>
            <SEO title="Blog home" />
            
            
            <Wrapper> 
                        
            <h1 style={{ textAlign: 'center', color: '#805D93' }}>Retrouver les Posts de Froggit:</h1>

            {nodes.map((e) => (
                <div style={{ padding: '10px 0', borderBottom: '1px, solid, #666'}}>
                    <h2>{e.frontmatter.title}</h2>
                          
                    <h3>Catégorie: {e.frontmatter.tags}</h3>
                    <h4>Langue: {e.frontmatter.language}</h4>
                    <p>{e.excerpt}</p>
                    <p>Temps de lecture: {e.timeToRead} min.</p>
                    <p>{e.tags}</p>
                    <Link to={e.fields.slug}>{e.frontmatter.title}</Link>
                </div>

            ))}

                <div style={{
                    padding: '20px 0',
                    display: 'flex',
                    justifyContent: 'space-bettwen',

                }}>
                {pageInfo, hasPreviousPage ? (
                    <Link
                    to={`${pageInfo.currentPage === 2 ? "/blog" : `/blog/${pageInfo.currentPage = 1}`}`} >
                        Previous 
                    </Link>
                ) : (
                <div />
                )}
                {pageInfo.hasNextPage && (
                    <Link to={`/blog/${pageInfo.currentPage}`}>Next</Link>
                )} 
</div>


            </Wrapper>
        </Layout>
    )
}
    
//limit: $limit
//skip: $skip
export const query = graphql`
query getPosts ($skip: Int!, $limit: Int!){
    site {
        siteMetadata {
          title
          description
        }
      }
    allMarkdownRemark(
        sort: { fields: frontmatter___date, order: DESC }
        limit: $limit
        skip: $skip
        filter: {
                fileAbsolutePath: { regex: "//content/posts//" }
                frontmatter: { published: { ne: false }, unlisted: { ne: true } }
              }) {
        nodes {
           
            frontmatter {
                title
                tags
                language
                slug
                generate_card
          language
          published
          slug
          tags
          imageShare
          unlisted
                
            
            }
            excerpt
            timeToRead
            fields {
            slug
            
            }
        }
        pageInfo {
            currentPage
            hasNextPage
            hasPreviousPage
            itemCount
            pageCount
            perPage
            totalCount
          }
    }
}

`
export default IndexPage


