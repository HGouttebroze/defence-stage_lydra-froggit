# Froggit page's URL:

https://froggit.gitlab.io/froggit.fr/

# Start developing.

Navigate into your new site’s directory and start it up.

```
npm install
npm start
```

## gatsby develop

Open the source code and start editing!
Your site is now running at http://localhost:8000!
Note: You'll also see a second link: http://localhost:8000/___graphql. This is a tool you can use to experiment with querying your data. Learn more about using this tool in the Gatsby tutorial.
Open the my-hello-world-starter directory in your code editor of choice and edit src/pages/index.js. Save your changes and the browser will update in real time!

### 🧐 What's inside?

A quick look at the top-level files and directories you'll see in a Gatsby project.

```
.
├── node_modules
├── src
├── .gitignore
├── .prettierrc
├── gatsby-browser.js
├── gatsby-config.js
├── gatsby-node.js
├── gatsby-ssr.js
├── LICENSE
├── package-lock.json
├── package.json
└── README.md
```

`/node_modules`: This directory contains all of the modules of code that your project depends on (npm packages) are automatically installed.

`/src`: This directory will contain all of the code related to what you will see on the front-end of your site (what you see in the browser) such as your site header or a page template. src is a convention for “source code”.

`.gitignore`: This file tells git which files it should not track / not maintain a version history for.

`.prettierrc`: This is a configuration file for Prettier. Prettier is a tool to help keep the formatting of your code consistent.

`gatsby-browser.js`: This file is where Gatsby expects to find any usage of the Gatsby browser APIs (if any). These allow customization/extension of default Gatsby settings affecting the browser.

`gatsby-config.js`: This is the main configuration file for a Gatsby site. This is where you can specify information about your site (metadata) like the site title and description, which Gatsby plugins you’d like to include, etc. (Check out the config docs for more detail).

`gatsby-node.js`: This file is where Gatsby expects to find any usage of the Gatsby Node APIs (if any). These allow customization/extension of default Gatsby settings affecting pieces of the site build process.

`gatsby-ssr.js`: This file is where Gatsby expects to find any usage of the Gatsby server-side rendering APIs (if any). These allow customization of default Gatsby settings affecting server-side rendering.

`LICENSE`: Gatsby is licensed under the MIT license.

`package-lock.json` (See package.json below, first). This is an automatically generated file based on the exact versions of your npm dependencies that were installed for your project. (You won’t change this file directly).

`package.json`: A manifest file for Node.js projects, which includes things like metadata (the project’s name, author, etc). This manifest is how npm knows which packages to install for your project.

`README.md`: A text file containing useful reference information about your project.

### 🎓 Learning Gatsby

Looking for more guidance? Full documentation for Gatsby lives on the website. Here are some places to start:

- **For most developers, we recommend starting with our [in-depth tutorial for creating a site with Gatsby](https://www.gatsbyjs.org/tutorial/).** It starts with zero assumptions about your level of ability and walks through every step of the process.

- **To dive straight into code samples, head [to our documentation](https://www.gatsbyjs.org/docs/).** In particular, check out the _Guides_, _API Reference_, and _Advanced Tutorials_ sections in the sidebar.

## Linter Gatsby

`.eslintrc.js`: ESLint is an open source JavaScript linting utility.
Gatsby ships with a built-in ESLint setup.
.eslintrc is the file that is create to use Eslint after run script:
node_modules/.bin/eslint -init
This file will determinate a few rules and configurations to your project.
The command to run ESLint in this project is: npm run lint

You can also create your own ESLint's configuration with NPM packages.

More informations on https://www.gatsbyjs.org/docs/eslint/

## Why ESLint and Prettier ?

ESLint is by far the most popular JavaScript linter. It statically analyzes your code to help you detect formatting issues and find code inconsistencies.

Prettier, while similar to ESLint by formatting your code, does not check your code quality. It just serves as a code formatter. It does this job pretty well though by natively supporting JavaScript but also JSX, Flow, TypeScript, HTML, JSON, CSS and many more languages.
Prettier is an opinionated code formatter. It enforces a consistent style by parsing your code and re-printing it with its own rules.

Even if they all have a similar purpose, they each excel in their own field.

All have similar objectives:
make code more consistent in itself and across team members and detect problematic code patterns that could lead to potential bugs.

1. commands in terminal:

eslint commands:

- to see errors: `npx eslint (file-name)`
  Warning: you can make this command, to target just files into src folder,
  (find this command in scripts of .gitlab-ci.yml)
- to fix errors: `npx eslint (file-name) --fix`
  Warning: like the last command, you can target files into src folder and fix
  linting errors with: `npm run fix`

prettier command to fix errors:
`npx prettier (file-name) --write`

2. install extention:

you can install and configue extentions for eslint and prettier in your main editor.

3. For us Eslint and Prettier together without conflicts:

First, we need to let Prettier do is job

In order to be able to use ESLint alongside Prettier, the idea is to deactivate all ESLint rules that might conflict with Prettier (code formatting rules).
Fortunately for us, the eslint-config-prettier package already does that.

We will rewrite our `.eslintrc` file by adding prettier to the extends array and removing any code formatting rules we had.

The prettier configuration will override any prior configuration in the extends array disabling all ESLint code formatting rules. With this configuration,
Prettier and ESLint can be run separately without any issues.

Next, we need to integrate Prettier with ESLint

The process of having to run two commands to lint and format our file is not very convenient. To fix this, we will integrate Prettier with ESLint by adding
the eslint-plugin-prettier package.

We will now rewrite our `.eslintrc` file by adding the prettier plugin in the plugins array and setting the newly established prettier rule to error so
that any prettier formatting error is considered as an ESLint error.

We can take an unlinted file and apply ESLint on it. They are marked by prettier/prettier as errors within our ESLint error output

Now let's use the fix option of ESLint.

Our file gets formatted the same way Prettier did.

## How to create new page

You can find [a documentation under the doc folder](doc/how_to_create_new_page.md).


## Gatsby Starter Catalyst

Basic starter that could be used as a barebones starting point for building a finished website. Implements `gatsby-theme-catalyst-core`, `gatsby-theme-catalyst-header-top`, and `gatsby-theme-catalyst-footer`.

**Documentation**

[Read the docs on Github](https://github.com/ehowey/gatsby-theme-catalyst)

**Catalyzing Start**

```sh
# create a new Gatsby site using the catalyst basic starter site
gatsby new catalyst https://github.com/ehowey/gatsby-starter-catalyst
```

[Read the Gatsby Quick Start Guide](https://www.gatsbyjs.org/docs/quick-start)

**Demo**

[https://gatsby-starter-catalyst.netlify.app/](https://gatsby-starter-catalyst.netlify.app/)

## About Gatsby Theme Catalyst

The Catalyst series of themes and starters for [GatsbyJS](https://www.gatsbyjs.org/) were designed to provide an opinionated set of integrated themes and starters that can be used to accelerate your next Gatsby project. The vision is for one "core" theme in which most dependencies and components are contained followed by progressively more styled and refined child themes and starters. These themes rely heavily on [Theme-UI](https://theme-ui.com/) and [MDX](https://mdxjs.com/getting-started/gatsby/).
